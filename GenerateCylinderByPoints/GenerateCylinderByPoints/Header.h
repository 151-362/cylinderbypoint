#pragma once

#include <conio.h>
#include <locale>

#include <math.h>
#include <afxtempl.h>

#include <fstream>
#include <iostream>
//#include <vector>

using namespace std;


/// <summary>
/// (x-x0)/a = (y-y0)/b = (z-z0)/c
/// </summary>
struct LineParametric	
{
	float a=0, b=0, c=0;
	float x0=0, y0=0, z0=0;
};

/// <summary>
/// �����. Ÿ ����������. ���������� �� ������� ��� ������������� ��� �������������
/// </summary>
struct Point
{
	float x=0, y=0, z=0;
	float length = 0;
};

/// <summary>
/// ���������. Ÿ ������������. Plane Equation:	Ax + By + Cz + D = 0
/// </summary>
struct Plane
{
	float A=0, B=0, C=0, D=0;
};