#pragma once

#include "Header.h"

int NormVector(double x, double y, double z)
{
	return 1.0 / sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}

int getCoordinate(double x1, double x2, double y1, double y2, double z1, double z2)
{
	return sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2) + pow((z2 - z1), 2));
}