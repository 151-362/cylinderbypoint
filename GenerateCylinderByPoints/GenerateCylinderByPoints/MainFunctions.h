#pragma once

#include "Header.h"

LineParametric VectorByTwoPoints(Point *point1, Point *point2);
void VectorNormalization(LineParametric *vector);
void LineOfPlanesIntersection(Plane *plane1, Plane *plane2, LineParametric *line);
void PlaneByThreePoints(Plane *plane, Point *point1, Point *point2, Point *point3);


//////////////////////////////////////////////////////////////////////////////////////////////////		GaussMethod

void PryamoiHod(int n, double **a, double *b)
{
	double v;
	for (int k = 0, i, j, im; k < n - 1; k++)
	{
		im = k;
		for (i = k + 1; i < n; i++)
		{
			if (fabs(a[im][k]) < fabs(a[i][k]))
			{
				im = i;
			}
		}
		if (im != k)
		{
			for (j = 0; j < n; j++)
			{
				v = a[im][j];
				a[im][j] = a[k][j];
				a[k][j] = v;
			}
			v = b[im];
			b[im] = b[k];
			b[k] = v;
		}
		for (i = k + 1; i < n; i++)
		{
			v = 1.0*a[i][k] / a[k][k];
			a[i][k] = 0;
			b[i] = b[i] - v * b[k];
			if (v != 0)
				for (j = k + 1; j < n; j++)
				{
					a[i][j] = a[i][j] - v * a[k][j];
				}
		}
	}
}

void ObratniHod(int n, double **a, double *b, double *x)
{
	double s = 0;
	x[n - 1] = 1.0*b[n - 1] / a[n - 1][n - 1];
	for (int i = n - 2, j; 0 <= i; i--)
	{
		s = 0;
		for (j = i + 1; j < n; j++)
		{
			s = s + a[i][j] * x[j];
		}
		x[i] = 1.0*(b[i] - s) / a[i][i];
	}
}

/// <summary>
/// ����� ������
/// </summary>
/// <param name="n">������ ���������� �������</param>
/// <param name="matrix">��������� �� �������</param>
/// <param name="freeMatrixVector">��������� �� ������ ��������� ������</param>
/// <param name="resultOfMatrix">��������� �� ������ ��� ���������</param>
void GaussMethod(int n, double **matrix, double *freeMatrixVector, double *resultOfMatrix)
{
	PryamoiHod(n, matrix, freeMatrixVector);
	ObratniHod(n, matrix, freeMatrixVector, resultOfMatrix);
}

//////////////////////////////////////////////////////////////////////////////////////////////////		// Miscellaneous

/// <summary>
/// ���������� ���������� ����� ����� �������
/// </summary>
/// <param name="point1">������ ����� (���������)</param>
/// <param name="point2">������ ����� (���������)</param>
double DistanceBetweenTwoPoints(Point *point1, Point *point2)
{
	return (double)sqrt(
		(point1->x - point2->x)*(point1->x - point2->x) +
		(point1->y - point2->y)*(point1->y - point2->y) +
		(point1->z - point2->z)*(point1->z - point2->z));
}

/// <summary>
/// ���������� ������ ���������� �� ��� ������ (XY) (���������� ���������� ������ ����������)
/// </summary>
/// <param name="point1">��������� �� ������ �����</param>
/// <param name="point2">��������� �� ������ �����</param>
/// <param name="point3">��������� �� ������ �����</param>
Point CircleCenterByThreePoints2D(Point *point1, Point *point2, Point *point3)
{
	Point lineCenter12, lineCenter23;
	Point vector12, vector23;

	lineCenter12.x = (point2->x + point1->x) / 2;	// points 1 and 2
	lineCenter12.y = (point2->y + point1->y) / 2;
	lineCenter23.x = (point2->x + point3->x) / 2;	// points 3 and 2
	lineCenter23.y = (point2->y + point3->y) / 2;

	vector12.x = point2->x - point1->x;		// points 1 and 2
	vector12.y = point2->y - point1->y;
	vector23.x = point2->x - point3->x;		// points 3 and 2
	vector23.y = point2->y - point3->y;

	Plane plane12, plane23;

	plane12.A = vector12.x;
	plane12.B = vector12.y;
	plane12.D = (-1)*plane12.A*lineCenter12.x + (-1)*plane12.B*lineCenter12.y;
	plane23.A = vector23.x;
	plane23.B = vector23.y;
	plane23.D = (-1)*plane23.A*lineCenter23.x + (-1)*plane23.B*lineCenter23.y;

	LineParametric line;
	Point pointCircleCenter;

	LineOfPlanesIntersection(&plane12, &plane23, &line);

	pointCircleCenter.x = line.x0;
	pointCircleCenter.y = line.y0;

	return pointCircleCenter;
}

/// <summary>
/// ���������� ������ ���������� �� ��� ������ � ������������ (���������� ���������� ������ ����������)
/// </summary>
/// <param name="point1">��������� �� ������ �����</param>
/// <param name="point2">��������� �� ������ �����</param>
/// <param name="point3">��������� �� ������ �����</param>
Point CircleCenterByThreePoints3D(Point *point1, Point *point2, Point *point3)
{
	Point lineCenter12, lineCenter23;
	Plane planeOverall;
	LineParametric vector12, vector23;
	
	PlaneByThreePoints(&planeOverall, point1, point2, point3);		// Overall Plane

	lineCenter12.x = (point2->x + point1->x) / 2;	// points 1 and 2
	lineCenter12.y = (point2->y + point1->y) / 2;
	lineCenter12.z = (point2->z + point1->z) / 2;
	lineCenter23.x = (point2->x + point3->x) / 2;	// points 3 and 2
	lineCenter23.y = (point2->y + point3->y) / 2;
	lineCenter23.z = (point2->z + point3->z) / 2;

	vector12.a = point2->x - point1->x;		// points 1 and 2
	vector12.b = point2->y - point1->y;
	vector12.c = point2->z - point1->z;
	vector23.a = point2->x - point3->x;		// points 3 and 2
	vector23.b = point2->y - point3->y;
	vector23.c = point2->z - point3->z;

	Plane plane12, plane23;

	plane12.A = vector12.a;
	plane12.B = vector12.b;
	plane12.C = vector12.c;
	plane12.D = (-1)*plane12.A*lineCenter12.x + (-1)*plane12.B*lineCenter12.y + (-1)*plane12.C*lineCenter12.z;
	plane23.A = vector23.a;
	plane23.B = vector23.b;
	plane23.C = vector23.c;
	plane23.D = (-1)*plane23.A*lineCenter23.x + (-1)*plane23.B*lineCenter23.y + (-1)*plane23.C*lineCenter23.z;


	int n = 3;
	double **matrix = (double **)malloc(n * sizeof(double));
	for (int i = 0; i < n; i++)
		matrix[i] = (double *)malloc(n * sizeof(double));

	double freeMatrixVector[3] = { (-1)*plane12.D, (-1)*plane23.D, (-1)*planeOverall.D };
	double resultOfMatrix[3];

	matrix[0][0] = plane12.A;
	matrix[0][1] = plane12.B;
	matrix[0][2] = plane12.C;

	matrix[1][0] = plane23.A;
	matrix[1][1] = plane23.B;
	matrix[1][2] = plane23.C;

	matrix[2][0] = planeOverall.A;
	matrix[2][1] = planeOverall.B;
	matrix[2][2] = planeOverall.C;

	GaussMethod(n, matrix, freeMatrixVector, resultOfMatrix);


	Point pointCircleCenter;


	pointCircleCenter.x = resultOfMatrix[0];
	pointCircleCenter.y = resultOfMatrix[1];
	pointCircleCenter.z = resultOfMatrix[2];

	return pointCircleCenter;
}

//////////////////////////////////////////////////////////////////////////////////////////////////		// Point

/// <summary>
/// ������������� ����� �� ��������� (���������� ��������������� �����)
/// </summary>
/// <param name="plane">��������� �� ���������</param>
/// <param name="pointOnPlane">��������� �� ����� �� ���������</param>
/// <param name="pointOutOfPlane">��������� �� ����� ��� ���������</param>
Point PointProjectionOnToPlane(Plane *plane, Point *pointOnPlane, Point *pointOutOfPlane)
{
	Point pointProjection;

	LineParametric vectorN;

	vectorN.a = plane->A;
	vectorN.b = plane->B;
	vectorN.c = plane->C;

	VectorNormalization(&vectorN);


	LineParametric 	vectorBetween;		// Vector between point on and out of plane
	
	vectorBetween = VectorByTwoPoints(pointOutOfPlane, pointOnPlane);

	double distance;		// Scalar distance from point to plane along the normal

	distance = vectorBetween.a*vectorN.a +
		vectorBetween.b*vectorN.b +
		vectorBetween.c*vectorN.c;

	pointProjection.x = pointOutOfPlane->x - distance * vectorN.a;
	pointProjection.y = pointOutOfPlane->y - distance * vectorN.b;
	pointProjection.z = pointOutOfPlane->z - distance * vectorN.c;

	return pointProjection;
}

/// <summary>
/// �������� - ����������� �� ����� ���������
/// </summary>
/// <param name="point">��������� �� ����� �� ���������</param>
/// <param name="plane">��������� �� ���������</param>
bool PointBelongsPlane(Point *point, Plane *plane)
{
	if (point->x*plane->A + point->y*plane->B + point->z*plane->C != (-1)*plane->D)
		return false;
	return true;
}

/// <summary>
/// ������������� ����� �� ������ (���������� ��������������� �����)
/// </summary>
/// <param name="point">��������� �� �����</param>
/// <param name="line">��������� �� �����</param>
Point PointProjectionToLine(Point *point, LineParametric *line)
{
	Plane planeWork;

	planeWork.A = line->a;
	planeWork.B = line->b;
	planeWork.C = line->c;
	planeWork.D = line->a*(-1)*(point->x) + line->b*(-1)*(point->y) + line->c*(-1)*(point->z);

	double lambda;

	lambda = (-1)*(planeWork.A*line->x0 + planeWork.B*line->y0 + planeWork.C*line->z0 + planeWork.D) /
			 (planeWork.A*line->a + planeWork.B*line->b + planeWork.C*line->c);

	Point pointProjection;

	pointProjection.x = line->x0 + line->a*lambda;
	pointProjection.y = line->y0 + line->b*lambda;
	pointProjection.z = line->z0 + line->c*lambda;

	pointProjection.length = DistanceBetweenTwoPoints(point, &pointProjection);
	
	pointProjection.length = pointProjection.length;
	return pointProjection;
}

/// <summary>
/// ������������� ����� � ������� �� �������� (������ ������� �����)
/// </summary>
/// <param name="point">��������� �� �����</param>
/// <param name="line">��������� �����</param>
/// <param name="planeXY">��������� �� ������� ���������</param>
void PointProjectionToPlaneFromLine(Point *point, LineParametric *line, Plane *plane)
{
	LineParametric lineVectorOffset;
	Plane planeFromLine;

	planeFromLine.A = line->a;
	planeFromLine.B = line->b;
	planeFromLine.C = line->c;
	planeFromLine.D = 0;

	LineOfPlanesIntersection(plane, &planeFromLine, &lineVectorOffset);

	VectorNormalization(&lineVectorOffset);

	point->x += point->length*lineVectorOffset.a;
	point->y += point->length*lineVectorOffset.b;
	point->z += point->length*lineVectorOffset.c;
}

//////////////////////////////////////////////////////////////////////////////////////////////////		// Vector

/// <summary>
/// ������������ �������.
/// </summary>
/// <param name="vector">��������� �� ������. ��������������� "a", "b", "c"</param>
void VectorNormalization(LineParametric *vector)
{
	vector->a /= sqrt(pow(vector->a, 2) + pow(vector->b, 2) + pow(vector->c, 2));
	vector->b /= sqrt(pow(vector->a, 2) + pow(vector->b, 2) + pow(vector->c, 2));
	vector->c /= sqrt(pow(vector->a, 2) + pow(vector->b, 2) + pow(vector->c, 2));
}

/// <summary>
/// ������ �� ���� �����.
/// </summary>
/// <param name="point1">��������� �� ���� ����� �������</param>
/// <param name="point2">��������� �� ���� ������ �������</param>
LineParametric VectorByTwoPoints(Point *point1, Point *point2)
{
	LineParametric vector;

	vector.a = point1->x - point2->x;
	vector.b = point1->y - point2->y;
	vector.c = point1->z - point2->z;

	return vector;
}

//////////////////////////////////////////////////////////////////////////////////////////////////		// Line

/// <summary>
/// ��������������� ��������� ������ ����������� ���� ����������
/// </summary>
/// <param name="plane1">������ ���������</param>
/// <param name="plane2">������ ���������</param>
/// <param name="line">��������� �� ������� �����</param>
void LineOfPlanesIntersection(Plane *plane1, Plane *plane2, LineParametric *line)
{
	line->a = plane1->B*plane2->C - plane2->B*plane1->C;
	line->b = plane1->C*plane2->A - plane2->C*plane1->A;
	line->c = plane1->A*plane2->B - plane2->A*plane1->B;

	int n = 2;
	double **matrix = (double **)malloc(n * sizeof(double));
	for (int i = 0; i < n; i++)
		matrix[i] = (double *)malloc(n * sizeof(double));
	double freeMatrixVector[2] = { (-1)*plane1->D, (-1)*plane2->D };
	double resultOfMatrix[2];

	if (fabs(line->a) != 0)
	{
		line->x0 = 0;

		matrix[0][0] = plane1->B;
		matrix[0][1] = plane1->C;
		matrix[1][0] = plane2->B;
		matrix[1][1] = plane2->C;
		GaussMethod(n, matrix, freeMatrixVector, resultOfMatrix);

		line->y0 = resultOfMatrix[0];
		line->z0 = resultOfMatrix[1];
	}
	else if (fabs(line->b) != 0)
	{
		line->y0 = 0;

		matrix[0][0] = plane1->A;
		matrix[0][1] = plane1->C;
		matrix[1][0] = plane2->A;
		matrix[1][1] = plane2->C;
		GaussMethod(n, matrix, freeMatrixVector, resultOfMatrix);

		line->x0 = resultOfMatrix[0];
		line->z0 = resultOfMatrix[1];
	}
	else if (fabs(line->c) != 0)
	{
		line->z0 = 0;

		matrix[0][0] = plane1->A;
		matrix[0][1] = plane1->B;
		matrix[1][0] = plane2->A;
		matrix[1][1] = plane2->B;
		GaussMethod(n, matrix, freeMatrixVector, resultOfMatrix);

		line->x0 = resultOfMatrix[0];
		line->y0 = resultOfMatrix[1];
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////		// Plane

/// <summary>
/// Ax+By+Cz+D=0 - ����� ������������� ��������� �� ��� ������
/// </summary>
/// <param name="plane">��������� �� ���������</param>
/// <param name="points">������ �����</param>
void PlaneByThreePoints(Plane *plane, Point *point1, Point *point2, Point *point3)
{
	plane->A = point1->y*(point2->z - point3->z) +
		point2->y*(point3->z - point1->z) +
		point3->y*(point1->z - point2->z);

	plane->B = point1->z*(point2->x - point3->x) +
		point2->z*(point3->x - point1->x) +
		point3->z*(point1->x - point2->x);

	plane->C = point1->x*(point2->y - point3->y) +
		point2->x*(point3->y - point1->y) +
		point3->x*(point1->y - point2->y);

	plane->D = (-1)*(
		point1->x*(point2->y*point3->z - point3->y*point2->z) +
		point2->x*(point3->y*point1->z - point1->y*point3->z) +
		point3->x*(point1->y*point2->z - point2->y*point1->z)
		);
}

//////////////////////////////////////////////////////////////////////////////////////////////////