#pragma once

#include "Header.h"

/// <summary>
/// ����������� �������������� ����������
/// </summary>
/// <param name="plane1">������ ���������</param>
/// <param name="plane2">������ ���������</param>
bool PlanesParallelism(Plane plane1, Plane plane2)
{
	float M1, M2, M3, M4, M5, M6;
	M1 = plane1.A*plane2.B - (plane1.B*plane2.A);
	M2 = plane1.A*plane2.C - (plane1.C*plane2.A);
	M3 = plane1.A*plane2.D - (plane1.D*plane2.A);
	M4 = plane1.B*plane2.C - (plane1.C*plane2.B);
	M5 = plane1.B*plane2.D - (plane1.D*plane2.B);
	M6 = plane1.C*plane2.D - (plane1.D*plane2.C);
	if ((M1 != 0) || (M2 != 0) || (M4 != 0))
	{
		return true;
	}
	else
	{
		if ((M3 == 0)&(M5 == 0)&(M6 == 0))
		{
			return true;
		}
		return false;
	}
	//if ((plane1.A / plane2.A == plane1.B / plane2.B) && (plane1.C / plane2.C == plane1.A / plane2.A) && (plane1.C / plane2.C == plane1.B / plane2.B))
	//{
	//	return true;
	//}
	//else
	//{
	//	return false;
	//}
}