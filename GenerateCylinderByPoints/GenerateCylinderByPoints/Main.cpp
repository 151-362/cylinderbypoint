
#include "Header.h"
#include "Vadim.h"
#include "Kirill.h"
#include "MainFunctions.h"
#include "Sergey.h"


int main()
{
	setlocale(LC_ALL, "Russian");

	ifstream file;
	file.open("data.txt");
	if (!file.is_open())
	{
		cout << "Problem with opening file \"data.txt\"";
		_getch();
		return 1;
	}

	CArray <Point, Point> points;
	Point point;

	while (!file.eof())							// Read points from file
	{
		file >> point.x >> point.y >> point.z;
		points.Add(point);
	}

	int firstPlanePoints[3], secondPlanePoints[3];
	Plane planeFirst, planeSecond;					// If A=B=C=0	 - It's not a plane
	bool flagPlanesParallelism = true;				// reverse

	for (int i1 = 0; i1 < points.GetSize() - 3; i1++)						// Find the parallel plane
	{
		for (int i12 = i1+1; i12 < points.GetSize() - 2; i12++)
		{
			for (int i13 = i12+1; i13 < points.GetSize() - 1; i13++)				 // last for FIRST plane
			{
				for (int i2 = i1 + 1; i2 < points.GetSize() - 2; i2++)
				{
					for (int i22 = i2 + 1; i22 < points.GetSize() - 1; i22++)
					{
						for (int i23 = i22 + 1; i23 < points.GetSize(); i23++)
						{
							PlaneByThreePoints(&planeFirst, &points[i1], &points[i12], &points[i13]);
							PlaneByThreePoints(&planeSecond, &points[i2], &points[i22], &points[i23]);
																									
							flagPlanesParallelism = PlanesParallelism(planeFirst, planeSecond);		
							if (!flagPlanesParallelism)
							{
								firstPlanePoints[0] = i1;
								firstPlanePoints[1] = i12;
								firstPlanePoints[2] = i13;
								secondPlanePoints[0] = i2;
								secondPlanePoints[1] = i22;
								secondPlanePoints[2] = i23;

								goto skip;
								//break;
							}
						}
					}
				}
			}
		}
	}
skip:
	if (flagPlanesParallelism)
	{
		cout << "Parallel planes not found";
		_getch();
		return 1;
	}

	//////////

	CArray <Point, Point> points�ylindricalFace;

	for (int i = 0; i < points.GetSize(); i++)						// Finding the �ylindrical Face's points
	{
		if (!PointBelongsPlane(&points[i], &planeFirst))
		{
			if (!PointBelongsPlane(&points[i], &planeSecond))
			{
				points�ylindricalFace.Add(points[i]);
			}
		}
	}

	for (int i = 0; i < points�ylindricalFace.GetSize(); i++)		// Projecting the �ylindrical Face's points to FirstPlane
	{
		points�ylindricalFace[i] = PointProjectionOnToPlane(&planeFirst, &points[firstPlanePoints[0]], &points�ylindricalFace[i]);
	}

	////////////

	// TODO
		// Find three points with MAX "X" and "Y" difference

	Point pointCircleCenterFirst, pointCircleCenterSecond;
	LineParametric	vectorAxis;
	double Radius = 0, Height = 0;

	pointCircleCenterFirst = CircleCenterByThreePoints3D(&points�ylindricalFace[0], &points�ylindricalFace[1], &points�ylindricalFace[2]);
	
	Radius = DistanceBetweenTwoPoints(&pointCircleCenterFirst, &points�ylindricalFace[0]);			// Radius

	////////
																								// Second Axis point																								

	pointCircleCenterSecond = PointProjectionOnToPlane(&planeSecond, &points[secondPlanePoints[0]], &pointCircleCenterFirst);

																								// Height and Axis Vector
	Height		= DistanceBetweenTwoPoints(&pointCircleCenterFirst, &pointCircleCenterSecond);
	vectorAxis	= VectorByTwoPoints(&pointCircleCenterFirst, &pointCircleCenterSecond);
	VectorNormalization(&vectorAxis);

	////////

	cout << "������ ��������: " << Radius << endl;
	cout << "������ ��������: " << Height << endl << endl;

	printf_s("������ ��� ��������: (%f, %f, %f)\n\n", 
		vectorAxis.a, vectorAxis.b, vectorAxis.c);
	printf_s("������ ����� �� ��� ��������: (%f, %f, %f)\n", 
		pointCircleCenterFirst.x, pointCircleCenterFirst.y, pointCircleCenterFirst.z);
	printf_s("������ ����� �� ��� ��������: (%f, %f, %f)\n", 
		pointCircleCenterSecond.x, pointCircleCenterSecond.y, pointCircleCenterSecond.z);

	_getch();
	return 0;
}